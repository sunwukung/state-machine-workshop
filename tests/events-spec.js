describe("withEvents", function () {

  var eventSubject,
    eventSubject_B;

  beforeEach(function () {
    eventSubject = K.withEvents({
      events: {
        foo: [],
        bar: []
      }
    });

    eventSubject_B = K.withEvents({
      events: {
        foo: [],
        bar: []
      }
    });

  });

  it("is a function", function () {
    expect(K.withEvents).to.be.a(Function);
  });


  describe("methods", function () {
    describe("on", function () {
      it("is a function", function () {
        expect(eventSubject.on).to.be.a(Function);
      });

      it("which will attach a callback to the key", function () {
        eventSubject.on("foo", function () {});
        expect(eventSubject.events["foo"].length).to.equal(1);
      });
    });


    describe("off", function () {
      it("is a function", function () {
        expect(eventSubject.off).to.be.a(Function);
      });

      describe("it will remove", function () {

        it("the callback from the key", function () {
          var subscriber = function () {};
          eventSubject.on("foo", subscriber);
          eventSubject.off("foo", subscriber);
          expect(eventSubject.events["foo"].length).to.equal(0);
        });

        it("all callbacks from the key if no callback specified", function () {
          eventSubject.on("foo", function () {});
          eventSubject.on("foo", function () {});
          eventSubject.on("foo", function () {});
          expect(eventSubject.events["foo"].length).to.equal(3);
          eventSubject.off("foo");
          expect(eventSubject.events["foo"].length).to.equal(0);
        });

        it("all callbacks if no key or callback specified", function () {
          eventSubject.on("foo", function () {});
          eventSubject.on("foo", function () {});
          eventSubject.on("bar", function () {});
          expect(eventSubject.events["foo"].length).to.equal(2);
          expect(eventSubject.events["bar"].length).to.equal(1);
          eventSubject.off();
          expect(eventSubject.events["foo"].length).to.equal(0);
          expect(eventSubject.events["bar"].length).to.equal(0);
        });

      })

    });

    describe("emit", function () {

      it("is a function", function () {
        expect(eventSubject.emit).to.be.a(Function);
      });

      it("calls all the callbacks on the given key with the data provided", function () {
        var callbackSpy_A = sinon.spy(),
          callbackSpy_B = sinon.spy();

        eventSubject.on("foo", callbackSpy_A);
        eventSubject.on("foo", callbackSpy_B);

        eventSubject.emit("foo", "tom", "dick", "harry");

        expect(callbackSpy_A.calledWithMatch("tom", "dick", "harry")).to.be(true);
        expect(callbackSpy_B.calledWithMatch("tom", "dick", "harry")).to.be(true);

        callbackSpy_A.reset();
        callbackSpy_B.reset();

      });

    });

    describe("once", function () {
      it("is a function", function () {
        expect(eventSubject.once).to.be.a(Function);
      });

      it("it adds a callback to the key", function () {
        eventSubject.once("foo", function () {});
        expect(eventSubject.events["foo"].length).to.equal(1);
      });

      it("will fire the callback once only", function () {
        var callbackSpy = sinon.spy();

        eventSubject.once("foo", callbackSpy);
        eventSubject.emit("foo");

        expect(callbackSpy.calledOnce).to.be(true);

        expect(eventSubject.events["foo"].length).to.equal(0);

        eventSubject.emit("foo");
        expect(callbackSpy.calledOnce).to.be(true);

      });

    });




  });


});