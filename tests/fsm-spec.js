describe.only("withFsm", function () {

  var fsmSubject,
    baseSpec = {
      states: {
        red    : ["green", "yellow"],
        yellow : ["red"],
        green  : ["yellow"]
      },
      actions: {
        next: [
          {from: "red", to: "yellow"},
          {from: "yellow", to: "green"},
          {from: "green", to: "red"},
        ]
      }
    };

  it("is an object", function () {
    expect(K.withFsm).to.be.a(Function);
  });

  beforeEach(function () {
    fsmSubject = K.withFsm({
      events: {}
    });
  });

  describe("methods", function () {

    describe("initialize", function () {
      it("is a function", function () {
        expect(fsmSubject.initialize).to.be.a(Function);
      });

      it("copies spec and initial to the subject", function () {
        fsmSubject.initialize(baseSpec, "red");
        expect(fsmSubject.states).to.equal(baseSpec.states);
        expect(fsmSubject.getStatus()).to.equal("red");
      });


      it("when called, it configures the event hub with state and action keys", function () {
        fsmSubject.initialize(baseSpec, "red");
        expect(fsmSubject.getKeys().sort()).to.eql(
          [
            "action:next",
            "state:green",
            "state:green:enter",
            "state:green:exit",
            "state:red",
            "state:red:enter",
            "state:red:exit",
            "state:yellow",
            "state:yellow:enter",
            "state:yellow:exit",
            // automatically added by machine
            "state:transition",
            "state:transition:enter",
            "state:transition:exit",
            "state:change",
            "state:error",
            "state:error:enter",
            "state:error:exit"
          ].sort());
      });
    });

    describe("canTransition", function () {

      it("is a function", function () {
        expect(fsmSubject.canTransition).to.be.a(Function);
      });

      it("will return true if the transition is possible", function () {
        fsmSubject.initialize(baseSpec, "yellow");
        expect(fsmSubject.canTransition("green")).to.be(true);
      });


      it("will return false if the transition is not possible", function () {
        fsmSubject.initialize(baseSpec, "red");
        expect(fsmSubject.canTransition("red")).to.be(false);
      });

      it("will return true if the conditions succeed", function () {
        var clonedSpec = _.cloneDeep(baseSpec),
          condition    = function () { return true; },
          conditionSpy = sinon.spy(condition);

        clonedSpec.conditions = {
          "red:yellow" : [
            conditionSpy
          ]
        };

        fsmSubject.initialize(clonedSpec, "red");
        expect(fsmSubject.conditions["red:yellow"].length).to.be(1);
        expect(fsmSubject.canTransition("yellow")).to.be(true);
        expect(conditionSpy.called).to.be(true);
      });

      it("will return false if the conditions succeed", function () {
        var clonedSpec = _.cloneDeep(baseSpec),
          condition    = function () { return false; },
          conditionSpy = sinon.spy(condition);

        clonedSpec.conditions = {
          "red:yellow" : [
            conditionSpy
          ]
        };

        fsmSubject.initialize(clonedSpec, "red");
        expect(fsmSubject.conditions["red:yellow"].length).to.be(1);
        expect(fsmSubject.canTransition("yellow")).to.be(false);
        expect(conditionSpy.called).to.be(true);
      });

    });

    describe("transition", function () {

      it("is a function", function () {
        expect(fsmSubject.transition).to.be.a(Function);
      });

      it("will transition the subject to the specified state if possible", function () {
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.transition("yellow");
        expect(fsmSubject.getStatus()).to.equal("yellow");
      });

      it("will cause the machine to emit on the transition, entry and exit - passing the context to the receivers", function () {
        var transitionSpy      = sinon.spy(),
            transitionEnterSpy = sinon.spy(),
            transitionExitSpy  = sinon.spy(),
            stateHistory = {
              current : "yellow",
              previous: "red"
            };

        fsmSubject.initialize(baseSpec, "red");

        fsmSubject.on("state:transition", transitionSpy);
        fsmSubject.on("state:transition:enter", transitionEnterSpy);
        fsmSubject.on("state:transition:exit", transitionExitSpy);

        fsmSubject.transition("yellow", "foo", "bar");

        expect(transitionSpy.calledWithMatch(stateHistory, "foo", "bar")).to.be(true);
        expect(transitionEnterSpy.calledWithMatch(stateHistory, "foo", "bar")).to.be(true);
        expect(transitionExitSpy.calledWithMatch(stateHistory, "foo", "bar")).to.be(true);
      });

      it("will cause the machine to emit on the state", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("state:yellow", transitionSpy);
        fsmSubject.transition("yellow");
        expect(transitionSpy.called).to.be(true);
      });

      it("will cause the machine to emit on the state change", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("state:change", transitionSpy);
        fsmSubject.transition("yellow");
        expect(transitionSpy.called).to.be(true);
      });

      it("will cause the machine to emit on exiting the state", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("state:red:exit", transitionSpy);
        fsmSubject.transition("yellow");
        expect(transitionSpy.called).to.be(true);
      });

      it("will cause the machine to emit on entering the state", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("state:yellow:enter", transitionSpy);
        fsmSubject.transition("yellow");
        expect(transitionSpy.called).to.be(true);
      });

    });

    describe("fire", function () {

      it("is a function", function () {
        expect(fsmSubject.fire).to.be.a(Function);
      });

      it("will fire the action", function () {
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.fire("next");
        expect(fsmSubject.getStatus()).to.be("yellow");
        fsmSubject.fire("next");
        expect(fsmSubject.getStatus()).to.be("green");
        fsmSubject.fire("next");
        expect(fsmSubject.getStatus()).to.be("red");
      });

      it("will cause the machine to transition", function () {
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.fire("next");
        expect(fsmSubject.getStatus()).to.equal("yellow");
      });

      it("will cause the machine to emit the transition", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("state:transition", transitionSpy);
        fsmSubject.fire("next");
        expect(transitionSpy.called).to.be(true);
      });

      it("will cause the machine to emit the action", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("action:next", transitionSpy);
        fsmSubject.fire("next");
        expect(transitionSpy.called).to.be(true);
      });

      it("will cause the machine to emit on the state", function () {
        var transitionSpy = sinon.spy();
        fsmSubject.initialize(baseSpec, "red");
        fsmSubject.on("state:yellow", transitionSpy);
        fsmSubject.fire("next");
        expect(transitionSpy.called).to.be(true);
      });

    });

    describe("getStatus", function () {
      it("is a function", function () {
        expect(fsmSubject.getStatus).to.be.a(Function);
      });
      it("it returns all the states mapped to boolean values", function () {
        fsmSubject.initialize(baseSpec, "red");
        expect(fsmSubject.getStatus()).to.be("red");
      });
    });

    describe("getAllStates", function () {
      it("is a function", function () {
        expect(fsmSubject.getAllStates).to.be.a(Function);
      });
      it("it returns all the states mapped to boolean values", function () {
        fsmSubject.initialize(baseSpec, "red");
        expect(fsmSubject.getAllStates()).to.eql({
          red: true,
          yellow: false,
          green: false,
          transition: false,
          error: false
        });
      });
    });

    describe("getActionTarget", function () {

      it("is a function", function () {
        expect(fsmSubject.getActionTarget).to.be.a(Function);
      });

      it("will fire the action", function () {
        fsmSubject.initialize(baseSpec, "red");
        expect(fsmSubject.getActionTarget("next")).to.equal("yellow");
      });

    });

    describe("it contains the event emitter methods", function () {
      _.each(["on", "off", "once", "emit"], function (method) {
        it(method, function () {
          expect(fsmSubject[method]).to.be.a(Function);
        });
      });
    });

  });


});