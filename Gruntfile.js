module.exports = function (grunt) {
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-concat");

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    // -------------------------------------------------------------------
    // WATCH
    // -------------------------------------------------------------------
    watch: {
      js: {
        files: [
          "Gruntfile.js",
          "./src/**/*.js"
        ],
        tasks: [
          "concat"
        ]
      },
      options: {
        atBegin: true
      }
    },
    // -------------------------------------------------------------------
    // CONCAT
    // -------------------------------------------------------------------
    concat: {
      vendor: {
        src: [
          './vendor/lodash.js',
          './vendor/jquery-1.11.1.min.js',
          './vendor/pathAnimator.js'
        ],
        dest: './dist/js/vendor.js'
      },
      lib: {
        src: [
          './src/events.js',
          './src/fsm.js'
        ],
        dest: './dist/js/lib.js',
      },
      presentation: {
        src: [
          './src/fsm/controls.js',
          './src/fsm/slides.js',
          './src/fsm/rpg.js',
          './src/fsm/player.js',
          './src/paths.js',
          './src/processSlides.js',
          './src/presentation.js'
        ],
        dest: './dist/js/presentation.js',
      }
    }
  });

  grunt.registerTask("default", ["watch"]);
  grunt.registerTask("testem", ["testem"]);

};