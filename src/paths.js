
var Paths = (function (paths) {
  var startSVG = "M53.86,221.071c19.186-0.188,52.14,1.679,67.64,0.429",
    roadSVG    = "M121.5,221.5c20.75-3.5,164.25-2.75,180.5,0",
    forestSVG  = "M302,221.5c19,0,17.304,16.317,15,29.5c-4.5,25.75-10.927,71.215-13.76,81.048 s-3.222,23.186-25.906,28.952c-9.833,2.5-39.13,5.602-65.583,6.5",
    meadowSVG  = "M211.75,367.5c-26.643,0.903-55.496-1.861-58.417-2.5c-12.583-2.75-33.833-12.334-35.833-38 c-1.366-17.536-6.553-58.131-9.5-83.5c-1.366-11.761,4.75-22,13.5-22",
    exitSVG    = "M302,221.5c20.5,2.5,220.194-7.791,251.86-5.291",
    start  = new PathAnimator(startSVG),
    road   = new PathAnimator(roadSVG),
    forest = new PathAnimator(forestSVG),
    meadow = new PathAnimator(meadowSVG),
    exit   = new PathAnimator(exitSVG),
    step = function (subject) {
      return function (point, angle) {
        subject.css({
          top  : (point.y - 103) + "px",
          left : (point.x - (86 / 2)) + "px"
        });
      };
    };

  paths = {
    start: function (subject, fin) {
      start.start(1, step(subject), false, 0, fin);
    },
    road: function (subject, fin) {
      road.start(1.5, step(subject), false, 0, fin);
    },
    forest: function (subject, fin) {
      forest.start(2, step(subject), false, 0, fin);
    },
    meadow: function (subject, fin) {
      meadow.start(2, step(subject), false, 0, fin);
    },
    exit: function (subject, fin) {
      exit.start(2, step(subject), false, 0, fin);
    }
  };

  return paths;

}(Paths || {}));

