processSlides = function (ready) {
  var slides,
    slideContent,
    screenInfo = {
      w: $(window).width(),
      h: $(window).height()
    };

  slides = $("section")
    .width(screenInfo.w)
    .height(screenInfo.h);

  slideContainer = $("#slide-container")
    .height(screenInfo.h)
    .width(screenInfo.w * slides.length);

  slideContent = $(".content");

  $.each(slideContent, function (i, content) {
    var $c = $(content),
      h    = $c.hasClass("rpg") ? 640 : $c.height(),
      top  = (screenInfo.h - h) / 2;

    if ($c.hasClass("rpg")) {
      var left = ($c.width() - 640) / 2;

      $("#rpg").css({
        "margin-left": left + "px",
        "margin-top" : 100 + "px"
      });
    }

    $c.css("margin-top", top + "px");
  });

  ready();
};