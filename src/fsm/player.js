var PlayerMachine = K.withFsm({});

PlayerMachine.initialize({
  states: {
    "stopped" : ["playing", "skipping"],
    "playing" : ["stopped", "skipping"],
    "skipping": ["playing"],
  },
  actions: {
    play: [
      {from: "stopped", to: "playing"},
      {from: "skipping", to: "playing"}
    ],
    stop: [
      {from: "playing", to: "stopped"}
    ],
    skip: [
      {from: "playing", to: "skipping"}
    ]
  }
}, "stopped");

TrackMachine = K.withFsm({});
TrackMachine.initialize({
  states: {
  "Walking_On_Sunshine": ["Into_The_Groove"],
  "Street_Life": ["Walking_On_Sunshine"],
  "Into_The_Groove": ["Street_Life"]
  },
  actions: {
    next: [
      {from: "Into_The_Groove", to: "Walking_On_Sunshine"},
      {from: "Walking_On_Sunshine", to: "Street_Life"},
      {from: "Street_Life", to: "Into_The_Groove"}
    ]
  }
}, "Walking_On_Sunshine");