var SlideMachine = K.withFsm({});

SlideMachine.initialize({
  states: {
    "introduction"     : ["fsm-explanation", "fsm-summary"],
    "fsm-explanation"  : ["fsm-terms", "introduction"],
    "fsm-terms"  : ["fsm-explanation", "fsm-illustration"],
    "fsm-illustration" : ["fsm-terms", "fsm-application"],
    "fsm-application"  : ["fsm-illustration", "examples"],
    "examples"  : ["fsm-application", "fsm-rpg"],
    "fsm-rpg"  : ["examples", "player"],
    "player"  : ["fsm-rpg", "summary"],
    "summary"  : ["player", "details"],
    "details"  : ["summary", "introduction"],
  },
  actions: {
    next: [
      {from: "introduction", to: "fsm-explanation"},
      {from: "fsm-explanation", to: "fsm-terms"},
      {from: "fsm-terms", to: "fsm-illustration"},
      {from: "fsm-illustration", to: "fsm-application"},
      {from: "fsm-application", to: "examples"},
      {from: "examples", to: "fsm-rpg"},
      {from: "fsm-rpg", to: "player"},
      {from: "player", to: "summary"},
      {from: "summary", to: "details"},
      {from: "details", to: "introduction"}
    ],
    prev: [
      {from: "introduction", to: "details"},
      {from: "details", to: "summary"},
      {from: "summary", to: "player"},
      {from: "player", to: "fsm-rpg"},
      {from: "fsm-rpg", to: "examples"},
      {from: "examples", to: "fsm-application"},
      {from: "fsm-application", to: "fsm-illustration"},
      {from: "fsm-illustration", to: "fsm-terms"},
      {from: "fsm-terms", to: "fsm-explanation"},
      {from: "fsm-explanation", to: "introduction"}
    ]
  }
}, "introduction");