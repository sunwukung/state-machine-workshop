var LocationMachine = K.withFsm({}),
  KnightMachine = K.withFsm({}),
  MonsterMachine = K.withFsm({}),
  PrincessMachine = K.withFsm({});


KnightMachine.initialize({
  states: {
    "waiting"  : ["*"],
    "walking"  : ["waiting", "victory"],
    "training" : ["waiting"],
    "fighting" : ["waiting"],
    "beaten"   : ["fighting"],
    "victory"  : ["fighting"]
  },
  actions: {
    wait: [
      {from: "walking",  to: "waiting"},
      {from: "victory", to: "waiting"},
      {from: "beaten", to: "waiting"},
      {from: "training", to: "waiting"}
    ],
    train: [
      {from: "waiting", to: "training"}
    ],
    walk: [
      {from: "waiting", to: "walking"}
    ],
    fight: [
      {from: "waiting", to: "fighting"}
    ],
    win: [
      {from: "fighting", to: "victory"}
    ],
    lose: [
      {from: "fighting", to: "beaten"}
    ]
  }
}, "waiting");


PrincessMachine.initialize({
  states: {
    "happy": ["sad"],
    "sad": ["sad"]
  }
}, "sad");


MonsterMachine.initialize({
  states: {
    "waiting" : ["waiting", "fighting"],
    "fighting": ["waiting"],
    "dead"    : ["fighting"]
  }
}, "waiting");


LocationMachine.initialize({
  states: {
    "start"   :["start"],
    "junction":["start", "dojo"],
    "battle"  :["junction"],
    "dojo"    :["battle"],
    "exit"    :["battle"]
  },
  actions: {
    next: [
      {from: "start", to: "junction"},
      {from: "junction", to: "battle"},
      {from: "battle", to: "dojo"},
      {from: "dojo", to: "junction"}
    ],
    victory: [
      {from: "battle", to: "exit"}
    ]
  }
}, "start");