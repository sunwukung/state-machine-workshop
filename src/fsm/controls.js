var ControlMachine = K.withFsm({});

ControlMachine.initialize({
  states: {
    "unlocked": ["locked"],
    "locked": ["unlocked"]
  }
},
"unlocked");