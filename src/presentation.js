var slideContainer,
  animateSlide,
  processSlides,
  danceInterval,
  toggleInterval,
  handleSlideAnimation,
  slideHandler,
  dance;

dance = function (elem) {
    danceInterval = setInterval(function () {
      console.log("dancing");
    }, 2000);
};

animateSlide = function (pos, mode) {
  var op = mode ? "-" : "",
    action = mode ? "next" : "prev";
  slideContainer.animate({
    "left": ["-", pos, "px"].join("")
  }, "fast", function () {
    ControlMachine.transition("unlocked");
    SlideMachine.fire(action);
  });
};

handleSlideAnimation = function (nextState, mode) {
  var slide = $(["#", nextState].join(""));

  if (ControlMachine.getStatus() === "unlocked" &&
    SlideMachine.canTransition(nextState)) {
    ControlMachine.transition("locked");
    animateSlide(slide.position().left, mode);
  }
};


processSlides(function () {

  var next   = $("#next"),
    prev     = $("#prev"),
    knight   = $("#knight"),
    princess = $("#princess"),
    monster  = $("#monster"),
    levelUp  = $("#levelUp"),
    roar     = $("#roar"),
    playerControls = {
      play: $("#play"),
      stop: $("#stop"),
      skip: $("#skip"),
    },
    tracks = {
      "Walking_On_Sunshine": document.getElementById("Walking_On_Sunshine"),
      "Street_Life": document.getElementById("Street_Life"),
      "Into_The_Groove": document.getElementById("Into_The_Groove")
    },
    strength = 0,
    playGame = true,
    audio = document.getElementById('audio'),
    slideHandler = handleSlideAnimation,
    routes = {
      "start:junction"  :"start",
      "junction:battle" :"road",
      "battle:exit"     :"exit",
      "battle:dojo"     :"forest",
      "dojo:junction"   :"meadow",
    },
    takeRoute,
    playRpg;


  /*
  -------------------------------------------
  RPG
  -------------------------------------------
   */
  takeRoute = function (knight, route, action) {
    KnightMachine.fire("walk");
    Paths[route](knight, function () {
      LocationMachine.fire(action);
    });
  };

  playRpg = function () {
    var action,
        route,
        currentLocation,
        nextLocation;

    if (KnightMachine.getStatus() === "waiting" && playGame) {
      currentLocation = LocationMachine.getStatus();
      nextLocation = LocationMachine.getActionTarget("next");
      route = routes[[currentLocation, nextLocation].join(":")];
      takeRoute(knight, route, "next");
    }
  };

  LocationMachine.on("state:change", function () {
    KnightMachine.fire("wait");
  });

  LocationMachine.on("state:battle", function () {
    KnightMachine.fire("fight");
  });

  LocationMachine.on("state:dojo", function () {
    KnightMachine.fire("train");
  });

  LocationMachine.on("state:exit", function () {
    var clear = _.keys(KnightMachine.getAllStates()).join(" ")
    next.removeClass(clear);
    prev.removeClass(clear);
    playGame = false;
    ControlMachine.transition("unlocked");
  });

  // add a condition branch
  KnightMachine.actions.resolve = [
    {from: "fighting", to: function () {
      return (strength === 2) ? "victory" : "beaten";
    }}
  ];

  KnightMachine.on("state:fighting", function () {
    console.log("started fighting");
    roar.addClass("visible");
    var t = setTimeout(function () {
      clearTimeout(t);
      roar.removeClass("visible");
      console.log("finished fighting");
      KnightMachine.fire("resolve");
    }, 1000);
  });

  KnightMachine.on("state:training", function () {
    console.log("started training");
    levelUp
      .html(strength + 1)
      .addClass("visible");

    var t = setTimeout(function () {
      clearTimeout(t);
      levelUp
        .html("")
        .removeClass("visible");

      strength = strength + 1;
      console.log("finished training, STR:", strength);
      KnightMachine.fire("wait");
    }, 1000);
  });

  KnightMachine.on("state:victory", function () {
    console.log("you just won the fight!");
    console.log(KnightMachine.getStatus());
    monster.removeClass("visible");
    KnightMachine.fire("wait");
    console.log(KnightMachine.getStatus());
    takeRoute(knight, routes["battle:exit"], "victory");
    slideHandler = handleSlideAnimation;
  });

  KnightMachine.on("state:beaten", function () {
    console.log("lost fight");
    KnightMachine.fire("wait");
  });

  KnightMachine.on("state:change", function (fn) {
    var clear = _.keys(
      KnightMachine.getAllStates()
    ).join(" ");

    next.removeClass(clear)
      .addClass(KnightMachine.getStatus());

    prev.removeClass(clear)
      .addClass(KnightMachine.getStatus());
  });

  /*
  -------------------------------------------
  AUDIO
  -------------------------------------------
   */


  playerControls.play.on("click", function () {
    if(PlayerMachine.canTransition("playing")) {
      var audio = tracks[TrackMachine.getStatus()];
      PlayerMachine.fire("play");
      audio.play();
    }
  });

  playerControls.stop.on("click", function () {
    if(PlayerMachine.canTransition("stopped")) {
      PlayerMachine.fire("stop");
      tracks[TrackMachine.getStatus()].pause();
    }
  });

  playerControls.skip.on("click", function () {
    if (PlayerMachine.canTransition("skipping")) {
      var audio = tracks[TrackMachine.getStatus()];
      audio.pause();
      TrackMachine.fire("next");
      PlayerMachine.fire("skip");
    }
  });

  PlayerMachine.on("state:skipping", function () {
      var audio = tracks[TrackMachine.getStatus()];
      PlayerMachine.fire("play");
      audio.play();
  });


  PlayerMachine.on("state:change", function () {
    console.log("PLAYER", PlayerMachine.getStatus());
    var stateMap = {
        stopped: {
          play: true,
          stop: false,
          skip: false
        },
        playing: {
          play: false,
          stop: true,
          skip: true
        },
        skipping: {
          play: false,
          stop: false,
          skip: false
        }
      },
      states = stateMap[PlayerMachine.getStatus()];

    _.each(states, function (value, key) {
      var status = value ? "enabled" : "disabled";
      playerControls[key].removeClass("enabled disabled")
        .addClass(status)
    });

  });



  /*
  -------------------------------------------
  SLIDES
  -------------------------------------------
   */

  ControlMachine.on("state:locked", function () {
    next.addClass("disabled");
    prev.addClass("disabled");
  });

  ControlMachine.on("state:unlocked", function () {
    next.removeClass("disabled");
    prev.removeClass("disabled");
  });


  // lock the controls on the fsm-rpg slide...
  SlideMachine.on("state:fsm-rpg", function () {
    console.log("PLAY?", playGame);
    if (playGame) {
      ControlMachine.transition("locked");
      slideHandler = function () {
        playRpg(knight, princess, monster)
      }
    }
  });

  next.on("click", function () {
    var nextState = SlideMachine.getActionTarget("next");
    slideHandler(nextState, true);
  });

  prev.on("click", function () {
    var nextState = SlideMachine.getActionTarget("prev");
    slideHandler(nextState, false);
  });


});








