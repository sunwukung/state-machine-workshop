/**
 * fsm.js
 *
 * intended to be used as a mixin
 * imparts state machine properties to host
 *
 * @author  marcus.kielly@gmail.com
 */
var K = (function (k) {

  /**
   * generates appropriate message keys
   * @param  {[type]} spec [description]
   * @return {[type]}      [description]
   */
  var compileEventKeys = function (spec) {
    var eventKeys = {},
      actionKeys = _.map(spec.actions, function (spec, action) {
        return ["action", action].join(":");
      });

    _.each(spec.states, function (stateSpec, k) {
      var key = ["state", k].join(":"),
          enterKey = ["state", k, "enter"].join(":"),
          exitKey  = ["state", k, "exit"].join(":");
      eventKeys[key] = true;
      eventKeys[enterKey] = true;
      eventKeys[exitKey] = true;
    });

  return _.keys(eventKeys)
    .concat(actionKeys)
    // add standard keys
    .concat(["state:error", "state:change"]);

  },
  compileArgs = function (key, data, currentState, targetState) {
    return [
        key,
        getStateHistory(currentState, targetState)
      ].concat(data);
  },
  getStateHistory = function (oldState, newState) {
    return {
      previous: oldState,
      current: newState
    };
  },
  emit = function (scope, key, args) {
    scope.emit.apply(scope, [key].concat(args));
  },
  executeTransition = function (scope, targetState, currentState, compiledArgs) {
    emit(scope, "state:" + scope.getStatus() + ":exit", compiledArgs);
    emit(scope, "state:transition", compiledArgs);
    emit(scope, "state:transition:enter", compiledArgs);
    scope.status = targetState;

    emit(scope, "state:" + targetState + ":enter", compiledArgs);
    if (scope.getStatus() === targetState) {
      emit(scope, "state:change", compiledArgs);
      emit(scope, "state:" + targetState, compiledArgs);
      emit(scope, "state:transition:exit", compiledArgs);
    }
  },
  /**
   * use this function to define states and transitions
   *
   * FORMAT:
   *
   * states: {
   *   red    : ["green", "yellow"],
   *   yellow : ["red"],
   *   green  : ["yellow"]
   * },
   *
   * actions: {
   *   next: [
   *     {from: "red", to: "yellow"},
   *     {from: "yellow", to: "green"}
   *     {from: "green", to: "red"},
   *   ],
   *   stop: [
   *     {from: "green", to: "red"}
   *     {from: "yellow", to: "red"}
   *   ]
   * }
   *
   * @param  {Object} spec
   * @param  {String} initial
   */
  initialize = function (spec, initial) {

    var eventKeys;

    // ensure event keys
    this.events = this.events || {};

    // ensure correct shape in spec
    spec.states     = spec.states || {};
    spec.actions    = spec.actions || {};
    spec.conditions = spec.conditions || {};

    // ensure transition and error states
    // append with wildcard "from" conditions
    spec.states.transition = ["*"];
    spec.states.error = ["*"];

    this.states     = spec.states;
    this.actions    = spec.actions;
    this.conditions = spec.conditions;
    this.status     = initial;
    this.initial    = initial;

    // register event keys based on spec
    eventKeys = compileEventKeys({
      states : spec.states,
      actions: spec.actions
    });

    // manually assign the event keys to the fsm
    _.each(eventKeys, function (key) {
      this.events[key] = [];
    }, this);
  },
  /**
   * return transition capability
   * @return {Boolean}
   */
  canTransition = function (targetState, compiledArgs) {
    var can = false,
      i = this.states[targetState] ? this.states[targetState].length : 0,
      transitionKey = [this.getStatus(), targetState].join(":"),
      j = this.conditions[transitionKey] ? this.conditions[transitionKey].length : 0;


    // ensure the state exists
    while (i--) {
      if (
          (this.states[targetState][i] === this.status) ||
          // allow transitions out of wildcard, error and transition states
          (this.states[targetState][i] === "*") ||
          (this.status === "error") ||
          (this.status === "transition")
        ) {
        can = true;
        break;
      }
    };

    // apply any conditions
    while (j--) {
      if (!this.conditions[transitionKey][j].apply(this, compiledArgs)) {
        can = false;
        break;
      }
    }

    return can;
  },
  /**
   * transition machine to a specified state if possible
   * @return {void 0}
   */
  transition = function () {
    var targetState = _.first(arguments),
        currentState = this.getStatus(),
        data = _.rest(arguments),
        self = this,
        compiledArgs = [getStateHistory(currentState, targetState)].concat(data);

    if (this.canTransition(targetState, compiledArgs)) {
      executeTransition(this, targetState, currentState, compiledArgs);
    } else {
      executeTransition(this, "error", currentState, compiledArgs);
      console.log("cannot perform transition:", targetState);
    }
  },
  /**
   * fire an event to transition the machine
   * @return {void 0}
   */
  fire = function () {
    var action = _.first(arguments),
        data   = _.rest(arguments),
        currentState = this.getStatus(),
        targetState = this.getActionTarget(action),
        compiledArgs = [getStateHistory(currentState, targetState)].concat(data);

    if (this.canTransition(targetState, compiledArgs)) {
      // divert to transition - publish for free
      executeTransition(this, targetState, currentState, compileArgs);
      this.emit("action:" + action, [
        {
          currentState: this.getStatus(),
          previousState: targetState
        }].concat(data));
    } else {
      executeTransition(this, "error", currentState, compiledArgs);
      console.log("cannot perform action:", action);
    }
  },
  /**
   * convenience method to return the intended
   * destination state for a given action
   * @param  {String} action
   * @return {String}
   * @todo   add capacity to process to: fn()
   */
  getActionTarget = function (action) {
    var target = "";
    _.each(this.actions[action], function (spec) {
      if (spec.from === this.status) {
        target = _.isFunction(spec.to) ? spec.to() : spec.to;
        return false;
      }
    }, this);
    return target;
  },
  /**
   * returns current machine status
   * indicates a problem with encapsulation
   * @return {String} [description]
   */
  getStatus = function () {
    return this.status;
  },
  /**
   * maps state keys to boolean values
   * @return {Object} [description]
   */
  getAllStates = function () {
    var allStates = {};
    _.each(_.keys(this.states), function (key, iter) {
      allStates[key] = this.getStatus() === key;
    }, this);
    return allStates;
  };

  k.withFsm = function (o) {
    o = k.withEvents(o);
    o.initialize = initialize;
    o.canTransition = canTransition;
    o.transition = transition;
    o.fire = fire;
    o.getStatus = getStatus;
    o.getAllStates = getAllStates;
    o.getActionTarget = getActionTarget;
    return o;
  };


  return k;

} (K || {}));
