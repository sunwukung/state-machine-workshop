/**
 * events.js
 *
 * intended to be used as a mixin
 * imparts classic event bus behaviour to host
 *
 * @author  marcus.kielly@gmail.com
 */
var K = (function (k) {

  var getKeys = function () {
      return _.keys(this.events);
    },
    on = function (key, cb) {
      if (this.events[key]) {
        this.events[key].push({ callback: cb, once: false });
      } else {
        console.warn("no event with name:", key);
      }
    },
    /**
     * remove cb from this.events[key]
     * @param  {String}   key
     * @param  {Function} cb
     * @return {void 0}
     */
    off = function (key, cb) {
      if (!key && !cb) {
        _.each(this.events, function (events, eventKey) {
          this.events[eventKey] = [];
        }, this);
        return;
      }

      if (key && this.events[key]) {

        if (key && cb) {
          this.events[key] = _.filter(this.events[key], function (evt) {
            return evt.callback !== cb;
          });
        }

        if (key && !cb) {
          this.events[key] = [];
        }

      } else {
        console.warn("no event with name:", key);
      }
    },
    /**
     * add a callback to this.events[key] which will only fire once
     * @param  {String}   key
     * @param  {Function} cb
     * @return {void 0}
     */
    once = function (key, cb) {
      if (this.events[key]) {
        this.events[key].push({ callback: cb, once: true });
      } else {
        console.warn("no event with name:", key);
      }
    },
    /**
     * emit data to all callbacks in this.events[key]
     * @return {void 0}
     */
    emit = function () {
      var key  = _.first(arguments),
          args = _.rest(arguments);

      _.each(this.events[key], function (evt) {
        evt.callback.apply(null, args);
        if (evt.once) {
          this.off(key, evt.callback);
        }
      }, this);
    };

  k.withEvents = function (o) {
    o.getKeys = getKeys;
    o.on = on;
    o.once = once;
    o.off = off;
    o.emit = emit;
    return o;
  };


  return k;

} (K || {}));
