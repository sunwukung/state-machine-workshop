# State Machine Workshop


Goals
to demonstrate and discuss the potential use of state machines to manipulate view state and various aspects of flow control


Stages

1: What is a state machine?
2: How do they work?
3: What can I use it for?

## What is a state machine?

A state machine is a term given to a software pattern borrowed from the electronics industry.
In laymans' terms, it takes a list of "states" and exposes methods to move the machine between
those states.
The classic example is a door. It has two states - "open" and "closed". Let's add another, "locked".
The door, in principle, can transition like this:

* "open" to "closed",
* "closed" to "open",
* "closed" to "locked",
* "locked" to "closed",

BUT the door is not allowed to go from "open" to "locked".
A state machine, then, is a means of representing a system in terms of it's states, and
presents a means of enforcing this logic.

## How do they work?


Summary
State machines can be used for a variety of tasks:

- controlling view state
- controlling control availability
- managing network interactions