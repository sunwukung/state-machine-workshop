var ControlMachine = K.withFsm({});

ControlMachine.initialize({
  states: {
    "unlocked": ["locked"],
    "locked": ["unlocked"]
  }
},
"unlocked");
var SlideMachine = K.withFsm({});

SlideMachine.initialize({
  states: {
    "introduction"     : ["fsm-explanation", "fsm-summary"],
    "fsm-explanation"  : ["fsm-terms", "introduction"],
    "fsm-terms"  : ["fsm-explanation", "fsm-illustration"],
    "fsm-illustration" : ["fsm-terms", "fsm-application"],
    "fsm-application"  : ["fsm-illustration", "examples"],
    "examples"  : ["fsm-application", "fsm-rpg"],
    "fsm-rpg"  : ["examples", "player"],
    "player"  : ["fsm-rpg", "summary"],
    "summary"  : ["player", "details"],
    "details"  : ["summary", "introduction"],
  },
  actions: {
    next: [
      {from: "introduction", to: "fsm-explanation"},
      {from: "fsm-explanation", to: "fsm-terms"},
      {from: "fsm-terms", to: "fsm-illustration"},
      {from: "fsm-illustration", to: "fsm-application"},
      {from: "fsm-application", to: "examples"},
      {from: "examples", to: "fsm-rpg"},
      {from: "fsm-rpg", to: "player"},
      {from: "player", to: "summary"},
      {from: "summary", to: "details"},
      {from: "details", to: "introduction"}
    ],
    prev: [
      {from: "introduction", to: "details"},
      {from: "details", to: "summary"},
      {from: "summary", to: "player"},
      {from: "player", to: "fsm-rpg"},
      {from: "fsm-rpg", to: "examples"},
      {from: "examples", to: "fsm-application"},
      {from: "fsm-application", to: "fsm-illustration"},
      {from: "fsm-illustration", to: "fsm-terms"},
      {from: "fsm-terms", to: "fsm-explanation"},
      {from: "fsm-explanation", to: "introduction"}
    ]
  }
}, "introduction");
var LocationMachine = K.withFsm({}),
  KnightMachine = K.withFsm({}),
  MonsterMachine = K.withFsm({}),
  PrincessMachine = K.withFsm({});


KnightMachine.initialize({
  states: {
    "waiting"  : ["*"],
    "walking"  : ["waiting", "victory"],
    "training" : ["waiting"],
    "fighting" : ["waiting"],
    "beaten"   : ["fighting"],
    "victory"  : ["fighting"]
  },
  actions: {
    wait: [
      {from: "walking",  to: "waiting"},
      {from: "victory", to: "waiting"},
      {from: "beaten", to: "waiting"},
      {from: "training", to: "waiting"}
    ],
    train: [
      {from: "waiting", to: "training"}
    ],
    walk: [
      {from: "waiting", to: "walking"}
    ],
    fight: [
      {from: "waiting", to: "fighting"}
    ],
    win: [
      {from: "fighting", to: "victory"}
    ],
    lose: [
      {from: "fighting", to: "beaten"}
    ]
  }
}, "waiting");


PrincessMachine.initialize({
  states: {
    "happy": ["sad"],
    "sad": ["sad"]
  }
}, "sad");


MonsterMachine.initialize({
  states: {
    "waiting" : ["waiting", "fighting"],
    "fighting": ["waiting"],
    "dead"    : ["fighting"]
  }
}, "waiting");


LocationMachine.initialize({
  states: {
    "start"   :["start"],
    "junction":["start", "dojo"],
    "battle"  :["junction"],
    "dojo"    :["battle"],
    "exit"    :["battle"]
  },
  actions: {
    next: [
      {from: "start", to: "junction"},
      {from: "junction", to: "battle"},
      {from: "battle", to: "dojo"},
      {from: "dojo", to: "junction"}
    ],
    victory: [
      {from: "battle", to: "exit"}
    ]
  }
}, "start");
var PlayerMachine = K.withFsm({});

PlayerMachine.initialize({
  states: {
    "stopped" : ["playing", "skipping"],
    "playing" : ["stopped", "skipping"],
    "skipping": ["playing"],
  },
  actions: {
    play: [
      {from: "stopped", to: "playing"},
      {from: "skipping", to: "playing"}
    ],
    stop: [
      {from: "playing", to: "stopped"}
    ],
    skip: [
      {from: "playing", to: "skipping"}
    ]
  }
}, "stopped");

TrackMachine = K.withFsm({});
TrackMachine.initialize({
  states: {
  "Walking_On_Sunshine": ["Into_The_Groove"],
  "Street_Life": ["Walking_On_Sunshine"],
  "Into_The_Groove": ["Street_Life"]
  },
  actions: {
    next: [
      {from: "Into_The_Groove", to: "Walking_On_Sunshine"},
      {from: "Walking_On_Sunshine", to: "Street_Life"},
      {from: "Street_Life", to: "Into_The_Groove"}
    ]
  }
}, "Walking_On_Sunshine");

var Paths = (function (paths) {
  var startSVG = "M53.86,221.071c19.186-0.188,52.14,1.679,67.64,0.429",
    roadSVG    = "M121.5,221.5c20.75-3.5,164.25-2.75,180.5,0",
    forestSVG  = "M302,221.5c19,0,17.304,16.317,15,29.5c-4.5,25.75-10.927,71.215-13.76,81.048 s-3.222,23.186-25.906,28.952c-9.833,2.5-39.13,5.602-65.583,6.5",
    meadowSVG  = "M211.75,367.5c-26.643,0.903-55.496-1.861-58.417-2.5c-12.583-2.75-33.833-12.334-35.833-38 c-1.366-17.536-6.553-58.131-9.5-83.5c-1.366-11.761,4.75-22,13.5-22",
    exitSVG    = "M302,221.5c20.5,2.5,220.194-7.791,251.86-5.291",
    start  = new PathAnimator(startSVG),
    road   = new PathAnimator(roadSVG),
    forest = new PathAnimator(forestSVG),
    meadow = new PathAnimator(meadowSVG),
    exit   = new PathAnimator(exitSVG),
    step = function (subject) {
      return function (point, angle) {
        subject.css({
          top  : (point.y - 103) + "px",
          left : (point.x - (86 / 2)) + "px"
        });
      };
    };

  paths = {
    start: function (subject, fin) {
      start.start(1, step(subject), false, 0, fin);
    },
    road: function (subject, fin) {
      road.start(1.5, step(subject), false, 0, fin);
    },
    forest: function (subject, fin) {
      forest.start(2, step(subject), false, 0, fin);
    },
    meadow: function (subject, fin) {
      meadow.start(2, step(subject), false, 0, fin);
    },
    exit: function (subject, fin) {
      exit.start(2, step(subject), false, 0, fin);
    }
  };

  return paths;

}(Paths || {}));


processSlides = function (ready) {
  var slides,
    slideContent,
    screenInfo = {
      w: $(window).width(),
      h: $(window).height()
    };

  slides = $("section")
    .width(screenInfo.w)
    .height(screenInfo.h);

  slideContainer = $("#slide-container")
    .height(screenInfo.h)
    .width(screenInfo.w * slides.length);

  slideContent = $(".content");

  $.each(slideContent, function (i, content) {
    var $c = $(content),
      h    = $c.hasClass("rpg") ? 640 : $c.height(),
      top  = (screenInfo.h - h) / 2;

    if ($c.hasClass("rpg")) {
      var left = ($c.width() - 640) / 2;

      $("#rpg").css({
        "margin-left": left + "px",
        "margin-top" : 100 + "px"
      });
    }

    $c.css("margin-top", top + "px");
  });

  ready();
};
var slideContainer,
  animateSlide,
  processSlides,
  danceInterval,
  toggleInterval,
  handleSlideAnimation,
  slideHandler,
  dance;

dance = function (elem) {
    danceInterval = setInterval(function () {
      console.log("dancing");
    }, 2000);
};

animateSlide = function (pos, mode) {
  var op = mode ? "-" : "",
    action = mode ? "next" : "prev";
  slideContainer.animate({
    "left": ["-", pos, "px"].join("")
  }, "fast", function () {
    ControlMachine.transition("unlocked");
    SlideMachine.fire(action);
  });
};

handleSlideAnimation = function (nextState, mode) {
  var slide = $(["#", nextState].join(""));

  if (ControlMachine.getStatus() === "unlocked" &&
    SlideMachine.canTransition(nextState)) {
    ControlMachine.transition("locked");
    animateSlide(slide.position().left, mode);
  }
};


processSlides(function () {

  var next   = $("#next"),
    prev     = $("#prev"),
    knight   = $("#knight"),
    princess = $("#princess"),
    monster  = $("#monster"),
    levelUp  = $("#levelUp"),
    roar     = $("#roar"),
    playerControls = {
      play: $("#play"),
      stop: $("#stop"),
      skip: $("#skip"),
    },
    tracks = {
      "Walking_On_Sunshine": document.getElementById("Walking_On_Sunshine"),
      "Street_Life": document.getElementById("Street_Life"),
      "Into_The_Groove": document.getElementById("Into_The_Groove")
    },
    strength = 0,
    playGame = true,
    audio = document.getElementById('audio'),
    slideHandler = handleSlideAnimation,
    routes = {
      "start:junction"  :"start",
      "junction:battle" :"road",
      "battle:exit"     :"exit",
      "battle:dojo"     :"forest",
      "dojo:junction"   :"meadow",
    },
    takeRoute,
    playRpg;


  /*
  -------------------------------------------
  RPG
  -------------------------------------------
   */
  takeRoute = function (knight, route, action) {
    KnightMachine.fire("walk");
    Paths[route](knight, function () {
      LocationMachine.fire(action);
    });
  };

  playRpg = function () {
    var action,
        route,
        currentLocation,
        nextLocation;

    if (KnightMachine.getStatus() === "waiting" && playGame) {
      currentLocation = LocationMachine.getStatus();
      nextLocation = LocationMachine.getActionTarget("next");
      route = routes[[currentLocation, nextLocation].join(":")];
      takeRoute(knight, route, "next");
    }
  };

  LocationMachine.on("state:change", function () {
    KnightMachine.fire("wait");
  });

  LocationMachine.on("state:battle", function () {
    KnightMachine.fire("fight");
  });

  LocationMachine.on("state:dojo", function () {
    KnightMachine.fire("train");
  });

  LocationMachine.on("state:exit", function () {
    var clear = _.keys(KnightMachine.getAllStates()).join(" ")
    next.removeClass(clear);
    prev.removeClass(clear);
    playGame = false;
    ControlMachine.transition("unlocked");
  });

  // add a condition branch
  KnightMachine.actions.resolve = [
    {from: "fighting", to: function () {
      return (strength === 2) ? "victory" : "beaten";
    }}
  ];

  KnightMachine.on("state:fighting", function () {
    console.log("started fighting");
    roar.addClass("visible");
    var t = setTimeout(function () {
      clearTimeout(t);
      roar.removeClass("visible");
      console.log("finished fighting");
      KnightMachine.fire("resolve");
    }, 1000);
  });

  KnightMachine.on("state:training", function () {
    console.log("started training");
    levelUp
      .html(strength + 1)
      .addClass("visible");

    var t = setTimeout(function () {
      clearTimeout(t);
      levelUp
        .html("")
        .removeClass("visible");

      strength = strength + 1;
      console.log("finished training, STR:", strength);
      KnightMachine.fire("wait");
    }, 1000);
  });

  KnightMachine.on("state:victory", function () {
    console.log("you just won the fight!");
    console.log(KnightMachine.getStatus());
    monster.removeClass("visible");
    KnightMachine.fire("wait");
    console.log(KnightMachine.getStatus());
    takeRoute(knight, routes["battle:exit"], "victory");
    slideHandler = handleSlideAnimation;
  });

  KnightMachine.on("state:beaten", function () {
    console.log("lost fight");
    KnightMachine.fire("wait");
  });

  KnightMachine.on("state:change", function (fn) {
    var clear = _.keys(
      KnightMachine.getAllStates()
    ).join(" ");

    next.removeClass(clear)
      .addClass(KnightMachine.getStatus());

    prev.removeClass(clear)
      .addClass(KnightMachine.getStatus());
  });

  /*
  -------------------------------------------
  AUDIO
  -------------------------------------------
   */


  playerControls.play.on("click", function () {
    if(PlayerMachine.canTransition("playing")) {
      var audio = tracks[TrackMachine.getStatus()];
      PlayerMachine.fire("play");
      audio.play();
    }
  });

  playerControls.stop.on("click", function () {
    if(PlayerMachine.canTransition("stopped")) {
      PlayerMachine.fire("stop");
      tracks[TrackMachine.getStatus()].pause();
    }
  });

  playerControls.skip.on("click", function () {
    if (PlayerMachine.canTransition("skipping")) {
      var audio = tracks[TrackMachine.getStatus()];
      audio.pause();
      TrackMachine.fire("next");
      PlayerMachine.fire("skip");
    }
  });

  PlayerMachine.on("state:skipping", function () {
      var audio = tracks[TrackMachine.getStatus()];
      PlayerMachine.fire("play");
      audio.play();
  });


  PlayerMachine.on("state:change", function () {
    console.log("PLAYER", PlayerMachine.getStatus());
    var stateMap = {
        stopped: {
          play: true,
          stop: false,
          skip: false
        },
        playing: {
          play: false,
          stop: true,
          skip: true
        },
        skipping: {
          play: false,
          stop: false,
          skip: false
        }
      },
      states = stateMap[PlayerMachine.getStatus()];

    _.each(states, function (value, key) {
      var status = value ? "enabled" : "disabled";
      playerControls[key].removeClass("enabled disabled")
        .addClass(status)
    });

  });



  /*
  -------------------------------------------
  SLIDES
  -------------------------------------------
   */

  ControlMachine.on("state:locked", function () {
    next.addClass("disabled");
    prev.addClass("disabled");
  });

  ControlMachine.on("state:unlocked", function () {
    next.removeClass("disabled");
    prev.removeClass("disabled");
  });


  // lock the controls on the fsm-rpg slide...
  SlideMachine.on("state:fsm-rpg", function () {
    console.log("PLAY?", playGame);
    if (playGame) {
      ControlMachine.transition("locked");
      slideHandler = function () {
        playRpg(knight, princess, monster)
      }
    }
  });

  next.on("click", function () {
    var nextState = SlideMachine.getActionTarget("next");
    slideHandler(nextState, true);
  });

  prev.on("click", function () {
    var nextState = SlideMachine.getActionTarget("prev");
    slideHandler(nextState, false);
  });


});








