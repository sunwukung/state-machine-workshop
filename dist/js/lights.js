var LightMachine = K.withFsm({events:{}}),
  LockMachine = K.withFsm({events:{}}),
  lights = $("#lights"),
  lightBulbs = {
    red   : $("#lights .light.red p"),
    yellow: $("#lights .light.yellow p"),
    green : $("#lights .light.green p"),
  },
  i = 0,
  limit = 0;

LightMachine.initialize({
  states: {
    red    : ["green", "yellow"],
    yellow : ["red"],
    green  : ["yellow"]
  },
  actions: {
    next: [
      {from: "red", to: "yellow"},
      {from: "yellow", to: "green"},
      {from: "green", to: "red"}
    ]
  },
  conditions: {
    "yellow:green": [
      function () {
        console.log("CALLING YELLOW TO GREEN CONDITION");
        return true;
      }
    ]
  }
}, "red");

LockMachine.initialize({
  states: {
    locked  : ["unlocked"],
    unlocked: ["locked"]
  },
  actions: {
    lock  : [{from: "unlocked", to: "locked"}],
    unlock: [{from: "locked", to: "unlocked"}]
  }
}, "unlocked");

LockMachine.on("state:error", function () {
  LockMachine.transition("unlocked");
  console.log("reset the lock", LockMachine.getStatus());
});

lights.removeClass("default red yellow green")
  .addClass(LightMachine.getStatus());

LightMachine.on("state:change", function (data, targetState, previousState) {
  lights.removeClass("default red yellow green");
  lights.addClass(LightMachine.getStatus());
});

LightMachine.on("state:green", function () {
  LockMachine.fire("lock");
  limit = 5;
});

LightMachine.on("state:yellow", function () {
  LockMachine.fire("lock");
  limit = 10;
});

LightMachine.on("state:red", function () {
  LockMachine.fire("lock");
  limit = 15;
});

LightMachine.on("state:error:enter", function () {
  $("#error").html("THERE WAS AN ERROR IN THE MACHINE");
  lightBulbs.red.html("!");
  lightBulbs.yellow.html("!");
  lightBulbs.green.html("!");
});

LightMachine.on("state:error:exit", function () {
  $("#error").html("");
  lightBulbs.red.html("0");
  lightBulbs.yellow.html("0");
  lightBulbs.green.html("0");
});

$("#next").on("click", function () {
  var lockStatus = LockMachine.getStatus();
  if (lockStatus === "unlocked") {
    LightMachine.fire("next");
  } else {
    i++;
    lightBulbs[LightMachine.getStatus()].html(i);
    if (i === limit) {
      LockMachine.fire("unlock");
      i = 0;
    }
  }
});
